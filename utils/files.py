from typing import List, Dict
from pathlib import Path

import os
import json
from .math import get_handicap_differential


def get_top(directory: str = 'rounds'):
    '''Get top rounds' handicap differentials

    Arguments:
        dir (str): 'rounds' directory where .json files are found

    Returns:
        top (List[float]): top handicap differentials based on scores.
            Will use this chart:

              Number of Handicap
            Differentials Available 	Differentials Used
            5 or 6                      Lowest 1
            7 or 8                      Lowest 2
            9 or 10                     Lowest 3
            11 or 12                    Lowest 4
            13 or 14                    Lowest 5
            15 or 16                    Lowest 6
            17                          Lowest 7
            18                          Lowest 8
            19                          Lowest 9
            20                          Lowest 10
    '''

    directory = Path(directory)

    rounds = []
    for rnd in directory.glob('**/*.json'):
        with rnd.open() as f:
            stats = json.load(f)
            if stats['holes_played'] == 9 or stats['holes_played'] == 18:
                rounds.append(stats)


def get_course_dict(course_name: str) -> Dict:
    '''Get the course dictionary from course name

    Arguments:
        course_name (str): course name

    Returns:
        course_dict (Dict): a dictionary containing course stats
    '''

    