from typing import List


def get_handicap_differential(scores: List[int],
                              rating: float,
                              slope: int) -> float:
    '''Get the handicap differential for a particular round

    Arguments:
        scores (List[int]): scores for holes
        rating (float): course rating
        slope (int): course slope

    Returns:
        handicap_diff (float): handicap differential
    '''

    total_score = sum(scores)
    handicap_diff = (total_score - rating)*113/rating

    # It's always rounded to one decimal place
    return round(handicap_diff, 1)
